<?php

namespace Infotechnohelp\PdfManager;

use Exception;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use RuntimeException;

/**
 * Class PdfManager
 */
class PdfManager
{
    // @todo Move to Infotechnohelp/FileSystem
    private function renderPhpContents(string $filePath)
    {
        if (is_file($filePath)) {
            ob_start();
            include($filePath);
            $content = ob_get_contents();
            ob_end_clean();
        } else {
            throw new RuntimeException(sprintf('Cant find view file %s!', $filePath));
        }

        return $content;
    }

    /**
     * @param string $htmlFolderPath
     * @param string|null $outputPath
     * @throws \Mpdf\MpdfException
     */
    public function renderFromHtml(string $htmlFolderPath, string $outputPath = null, $download = false)
    {
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($this->renderPhpContents($htmlFolderPath . 'index.php'));

        $destination = (is_null($outputPath)) ? Destination::INLINE : Destination::FILE;
        $outputPath = (is_null($outputPath)) ? 'invoice.pdf' : $outputPath;

        $destination = (!$download) ? $destination : Destination::DOWNLOAD;

        $mpdf->Output($outputPath, $destination);
    }

    // @todo Move to Infotechnohelp/FileSystem
    private function extractFilename(string $filePath)
    {
        return substr($filePath, strrpos($filePath, '/') + 1);
    }
    

    /**
     * @return \setasign\Fpdi\Fpdi
     */
    private function initPdfFile()
    {
        return new \setasign\Fpdi\Fpdi();
    }

    /**
     * @param array $filePaths
     * @param string $outputFilePath
     * @return bool
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\Filter\FilterException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function mergePdf(array $filePaths, string $outputFilePath)
    {
        $pdf = $this->initPdfFile();

        foreach ($filePaths as $filePath) {
            $pageCount = $pdf->setSourceFile($filePath);

            for ($i = 1; $i <= $pageCount; $i++) {
                $pdf->AddPage();
                $pdf->useTemplate($pdf->importPage($i));
            }
        }

        try {
            $pdf->Output($outputFilePath, "F");
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            return false;
        }

        return true;
    }

    /**
     * @param string $originalFilePath
     * @param string $outputDirPath
     * @param array|null $pageChunks
     * @return array
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\Filter\FilterException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function splitPdf(string $originalFilePath, string $outputDirPath, array $pageChunks = null): array
    {
        $pdf = $this->initPdfFile();

        $pageCount = $pdf->setSourceFile($originalFilePath);

        $fileName = $this->extractFilename($originalFilePath);

        $result = [];

        if (empty($pageChunks)) {

            for ($i = 1; $i <= $pageCount; $i++) {
                $newPdf = $this->initPdfFile();
                $newPdf->AddPage();
                $newPdf->setSourceFile($originalFilePath);
                $newPdf->useTemplate($newPdf->importPage($i));

                try {
                    $newFileName = $outputDirPath . DIRECTORY_SEPARATOR .
                        str_replace('.pdf', '', $fileName) . '_' . $i . ".pdf";
                    $newPdf->Output($newFileName, "F");
                    $result[] = $newFileName;
                } catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return [];
                }
            }

            return $result;
        }

        foreach ($pageChunks as $pageChunk) {

            $newPdf = $this->initPdfFile();
            $newPdf->setSourceFile($originalFilePath);

            for ($i = $pageChunk[0]; $i <= $pageChunk[1]; $i++) {
                $newPdf->AddPage();
                $newPdf->useTemplate($newPdf->importPage($i));
            }

            try {
                $newFileName = $outputDirPath . DIRECTORY_SEPARATOR .
                    str_replace('.pdf', '', $fileName) . '_' . $pageChunk[0] . '-' . $pageChunk[1] . ".pdf";
                $newPdf->Output($newFileName, "F");
                $result[] = $newFileName;
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return [];
            }
        }

        return $result;
    }


}