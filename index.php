<?php

require __DIR__ . '/vendor/autoload.php';

require_once 'src/PdfManager.php';

$PdfManager = new \Infotechnohelp\PdfManager\PdfManager();

$result = $PdfManager->splitPdf('test/input/filename.pdf', 'test/split/filename/', [
    [1, 2],
    [4, 5],
]);

$PdfManager->mergePdf($result, 'test/merged/filename_merged.pdf');
